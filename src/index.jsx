import { StrictMode } from 'react';
import { createRoot } from 'react-dom/client';
import './index.css';
import { Tasks } from './pages/tasks';
import {
	createBrowserRouter,
	createRoutesFromElements,
	Route,
	RouterProvider,
} from 'react-router-dom';

const container = document.getElementById('root');

// Create a root.
const root = createRoot(container);

// Create router
const router = createBrowserRouter(
	createRoutesFromElements(<Route path="/" element={<Tasks />} />)
);

// Initial render
root.render(
	<StrictMode>
		<RouterProvider router={router} />
	</StrictMode>
);
