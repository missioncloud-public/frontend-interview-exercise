/** @type {import('ts-jest').JestConfigWithTsJest} */

module.exports = {
	moduleNameMapper: {
		'^.+\\.(svg)$': '<rootDir>/config/mockSvg.tsx',
	},
	setupFilesAfterEnv: ['<rootDir>/config/jest.setupTests.js'],
	testEnvironment: 'jsdom',
	transform: {
		'.*\\.(jsx|js|ts|tsx)$': [
			'@swc/jest',
			{
				jsc: {
					transform: {
						react: {
							runtime: 'automatic',
						},
					},
				},
			},
		],
		'.+\\.(svg|css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
			'jest-transform-stub',
	},
	verbose: true,
};
